# Configurar el proyecto de Google Cloud Platform

1. Cree un proyecto en Google Cloud Platform.
Active las Cloud Functions API en el proyecto.
Cree una Cloud Function dummy con el nombre que desee.
Configurar GitLab CI

2. Cree un archivo .gitlab-ci.yml en la raíz del repositorio.

3. Configure el archivo .gitlab-ci.yml con los siguientes pasos:
stages:
  - deploy

deploy:
  stage: deploy
  image: google/cloud-sdk
  script:
    - gcloud auth activate-service-account --key-file <path-to-your-service-account-key-file>
    - gcloud config set project <your-gcp-project-id>
    - gcloud functions deploy <your-function-name> --source=. --region= --runtime <runtime-environment> --trigger-http

Reemplace <path-to-your-service-account-key-file>, <your-gcp-project-id> y <your-function-name> con los valores correspondientes.

Asegúrese de tener un archivo de credenciales de servicio de GCP. Deberá agregar este archivo a los Secretos de GitLab como se indica en el siguiente paso.

Configurar los Secretos de GitLab

Vaya a Configuración > CI/CD > Secretos en GitLab.
Cree dos nuevos secretos: GCP_PROJECT_ID y GCP_SA_KEY.
Para el valor de GCP_PROJECT_ID, ingrese el ID de su proyecto de GCP.
Para el valor de GCP_SA_KEY, pegue el contenido del archivo de credenciales de servicio de GCP en el campo de valor.
Ejecutar el pipeline

Haga un push de sus cambios al repositorio.
Vaya a Pipelines en GitLab y asegúrese de que se haya iniciado un pipeline.
Espere a que el pipeline se complete. Si todo está configurado correctamente, su Cloud Function debería estar desplegada y lista para usar.
Con estos pasos, tendría un pipeline de CI/CD que despliega su Cloud Function en GCP cada vez que se hace un push al repositorio. Por supuesto, es posible que deba ajustar los detalles de este ejemplo para que se adapte a su caso específico.